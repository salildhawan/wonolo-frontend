
$(document).ready(function () {
    $("header").find(".menu").click(function () {
        $(".nav-main").slideToggle();
    })
});

$(document).ready(function () {
    var owl = $('.owl-carousel');
    owl.owlCarousel({

        // itemsCustom : false,
        // itemsDesktop : [1199,4],
        // itemsDesktopSmall : [980,2],
        // itemsTablet: [768,1],
        // itemsTabletSmall: false,
        // itemsMobile : [414,1],
        // singleItem : true,
        // itemsScaleUp : false,


         autoplay:false,
         loop: true,
         margin:-300,
         nav: false,
         Type: Number,
         Default: 3,
        dots: true,
        responsive: {
            0: {
                items: 1
                

            },
            200: {
                items: 1,
                margin:0,
            },
            400: {
                items: 1,
                margin:0,
            },
            600: {
                items: 1,
                margin:0,
            },
            1000: {
                items: 1
            }
        }
    })
})